<?php
/*

For best results with GoogleMaps, use XHTML compliant web pages with this header:

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml">

For database caching, you will want to use this schema:

			// this is the one we are using for drupal
            CREATE TABLE {rlisting_geocode} (
                nid                 INT UNSIGNED NOT NULL,
                lon                 FlOAT DEFAULT NULL,
                lat                 FlOAT DEFAULT NULL,
                PRIMARY KEY         (nid)
            )

*/

class ListingMap {
	//var $google_key = "ABQIAAAAwcQhTAXE-Zof5561C-WswBR17Bi3C0ZODvjKTxswnPTAST7JZRTtqD3wiNr5bwJgL4bngWPxF9jPlg"; // rlisting.ken.local
	//var $google_key = "ABQIAAAAwcQhTAXE-Zof5561C-WswBRmR3zueYwC7QYbstsYYQaCeYD75RRZ5MW95is-dM219vUQc878Q84LvQ"; // rlisting.netriftsolutions.com
	var $google_key;
	var $map_id;
	var $_db_cache_table = "rlisting_geocode";
	var $_markers = array();
	var $max_lon;
	var $min_lon;
	var $max_lat;
	var $min_lat;
	var $center_lon;
	var $center_lat;
	// map contro ltype , large = move/center/zoom controls
	var $control_size = 'large'; 
	// (map/satellite/hybrid)
	var $type_controls = true; 
	// G_NORMAL_MAP/G_SATELLITE_MAP/G_HYBRID_MAP
	var $map_type = 'G_NORMAL_MAP'; 
	var $scale_control = true; 


	// constructor
	function ListingMap(){
		// grab the key from the admin setting 
		$this->google_key = variable_get('listing_google_key','');
	}

	function setMapID($id){
		$this->map_id = $id;
	}

	function getHeaderJS(){
	        //return 'http://maps.google.com/maps?file=api&amp;v=2&amp;key='.$this->google_key; 
	        return sprintf('<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=%s" type="text/javascript" charset="utf-8"></script>', $this->google_key);

	}

	function addMarkerByAddress($node,$title = '',$html = '', $tooltip=' '){
		if (($_geocode = $this->getGeocode($node)) == false)
			return false;
		return $this->addMarkerByCoords($_geocode['lon'],$_geocode['lat'],$title,$html,$tooltip);
	}
    function addMarkerByCoords($lon,$lat,$title = '',$html = '',$tooltip = '') {
        $_marker['lon'] = $lon;
        $_marker['lat'] = $lat;
        $_marker['html'] = (is_array($html) || strlen($html) > 0) ? $html : $title;
        $_marker['title'] = $title;
        $_marker['tooltip'] = $tooltip;
        $this->_markers[] = $_marker;
        $this->adjustCenterCoords($_marker['lon'],$_marker['lat']);
        // return index of marker
        return count($this->_markers) - 1;
    }
    function adjustCenterCoords($lon,$lat) {
        if(strlen((string)$lon) == 0 || strlen((string)$lat) == 0)
            return false;
        $this->_max_lon = (float) max($lon, $this->_max_lon);
        $this->_min_lon = (float) min($lon, $this->_min_lon);
        $this->_max_lat = (float) max($lat, $this->_max_lat);
        $this->_min_lat = (float) min($lat, $this->_min_lat);

        $this->center_lon = (float) ($this->_min_lon + $this->_max_lon) / 2;
        $this->center_lat = (float) ($this->_min_lat + $this->_max_lat) / 2;
        return true;
    }

    function getGeocode($node) {
        if(empty($node))
            return false;

        $_geocode = false;

        if(($_geocode = $this->getCache($node->nid)) === false) {
			$address = $node->address ." ". $node->city_name . " " . 
						$node->prov_name . " " . $node->postal_zip . " " . $node->country_name;
					
            if(($_geocode = $this->geoGetCoords($address)) !== false) {
                $this->putCache($node->nid, $_geocode['lon'], $_geocode['lat']);
            }
        }

        return $_geocode;
    }
	function getCache($node_id){
		$_ret = array();
        $SQL = "SELECT lon,lat 
				FROM {".$this->_db_cache_table."}
                WHERE nid = %d
        ";
        $_res = db_query($SQL, $node_id);
        if($_row = db_fetch_object($_res)) {
            $_ret['lon'] = $_row->lon;
            $_ret['lat'] = $_row->lat;
        }
        return !empty($_ret) ? $_ret : false;
	}
    function putCache($node_id, $lon, $lat) {
        if(($node_id) <= 0 || strlen($lon) == 0 || strlen($lat) == 0)
           return false;
        $SQL = "INSERT INTO {".$this->_db_cache_table."}
                (nid,lon,lat) VALUES (%d,%f,%f)
        ";

        $_res = db_query($SQL,$node_id, $lon, $lat);
        return true;
    }
    function geoGetCoords($address,$depth=0) {
		$_url = sprintf('http://%s/maps/geo?&q=%s&output=csv&key=%s','maps.google.com',rawurlencode($address),$this->google_key);
       	$_result = false;

       	if($_result = file_get_contents($_url)) {
       		$_result_parts = explode(',',$_result);
           		if($_result_parts[0] != 200)
               		return false;

              	$_coords['lat'] = $_result_parts[2];
               	$_coords['lon'] = $_result_parts[3];
 		}

		return $_coords;
	}

	function enableScaleControl(){
		$this->scale_control = true;
	}
	function disableScaleControl(){
		$this->scale_control = true;
	}
	function enableTypeControl(){
		$this->type_control = true;
	}
	function disableTypeControl(){
		$this->type_control = true;
	}
    function setMapType($type) {
        switch($type) {
            case 'hybrid':
                $this->map_type = 'G_HYBRID_MAP';
                break;
            case 'satellite':
                $this->map_type = 'G_SATELLITE_MAP';
                break;
            case 'map':
            default:
                $this->map_type = 'G_NORMAL_MAP';
                break;
        }      
    }  

	function getMapJS(){
		$location = $this->_markers[0];
		$center_lat = $location['lat'];
		$center_lon = $location['lon'];
		
		if($this->control_size == 'large'){
			$_control .= 'map.addControl(new GLargeMapControl());' . "\n";
		}else{
			$_control .= 'map.addControl(new GSmallMapControl());' . "\n";
		};
		if($this->type_controls){
			$_control .= 'map.addControl(new GMapTypeControl());' . "\n";
		};

		return "
      		if (GBrowserIsCompatible()) {
        		var map = new GMap2(document.getElementById(\"map_canvas\"));
        		var center = new GLatLng($center_lat, $center_lon);
        		map.setCenter(center, 12);
				$_control

				var marker = new GMarker(center);
				map.addOverlay(marker);
      		}
		";
	}
};
