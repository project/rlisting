<?php
//----------------------------------------------------------------------------
/* Generate the contact form for user
 *
 * @param $user_id
 *	The id of the user we want to send an emai to.
 */
function listing_contact_form($user_id,$listing_id){
	global $user; // currently logged in user

	$agent = user_load(array('uid'=>$user_id));
	$agent_name = "<h2>To: ".$agent->first_name." ".$agent->last_name."(".$agent->name.")</h2>";


	$form['send_to'] = array(
		'#type'		=> 'markup',
		'#value'	=> $agent_name,
		'#weight'	=> 2,
	);
	$form['send_to_uid'] = array(
		'#type'		=> 'hidden',
		'#value'	=> $agent->uid,
	);
	if ($user->uid > 0){
		$user = user_load(array('uid'=>$user->uid));
	
		// user is logged in. so fill in information
		$user_name = "<h2>From: ".$user->first_name." ".$user->last_name."(".$user->name.")</h2>";
		$form['from_name'] = array(
			'#type'		=> 'markup',
			'#value'	=> $user_name,
			'#weight'	=> 2,
		);
		// we dont' want to show the email addresss
		$form['from_uid'] = array(
			'#type'		=> 'hidden',
			'#value'	=> $user->uid,
		);
	}else{
		$form['from_name'] = array(
			'#type'		=> 'textfield',
			'#title'	=> t("Your Name"),
			'#required'	=> TRUE,
			'#default_value'	=> $node->title,
			'#weight'	=> 3,
		);
		$form['from_email'] = array(
			'#type'		=> 'textfield',
			'#title'	=> t("Your Email"),
			'#required'	=> TRUE,
			'#default_value'	=> $node->title,
			'#weight'	=> 3,
		);
	};
	// get the listing subject
	$listing = node_load(array('nid'=>$listing_id));
	$form['subject'] = array(
		'#type'		=> 'textfield',
		'#title'	=> t("Subject"),
		'#required'	=> TRUE,
		'#default_value'	=> "Listing #".$listing->nid.": ".$listing->title,
		'#weight'	=> 4,
		'#size'	=> '85%',
	);
	$form['message'] = array(
		'#type'		=> 'textarea',
		'#title'	=> t("Message"),
		'#default_value'	=> $node->body,
		'#rows'		=> 10,
		'#required'	=> TRUE,
		'#weight'	=> 5,
	);
	$form['submit'] = array(
		'#type'	=> 'submit',
		'#value'	=> 'Send e-mail',
		'#weight'	=> 8,
	);


	return $form;
};
//----------------------------------------------------------------------------
function listing_contact_form_validate($form_id,$form_values){

	// we have an anonymous user
	if($form_values['from_uid'] <= 0){
		if (valid_email_address($form_values['from_email']) == false){
			form_set_error('from_email',t('Valid e-mail address required'));	
		};
	};
}
function listing_contact_form_submit($form_id,$form_values){
	// now send an email

	// who are we sending it to? 
	$to_uid = $form_values['send_to_uid']; 
	$agent = user_load(array('uid'=>$to_uid));
	$to = $agent->mail;

	// from whom
	if ($form_values['from_uid'] > 0){
		// sender is logged in.
		$from_uid = $form_values['from_uid'];
		$sender = user_load(array('uid' => $from_uid));
		$from_mail = $sender->mail;
		$from_name = $sender->first_name." ".$sender->last_name;
	}else{
		$from_mail = $form_values['from_email'];
		$from_name = $form_values['from_name'];
	};
	$from = "$from_name <$from_mail>";
	$subject = $form_values['subject'];
	$body = $form_values['message'];

	drupal_mail('contact_agent',$to,$subject,$body,$from);


}

//----------------------------------------------------------------------------
function listing_form($node){
	global $user;
	// make sure the listing_max is loaded for the currently logged in user.
	$user = user_load(array('uid'=>$user->uid));

	if ($user->listing_max > rlistingapi_listing_num($user->uid) ||
		user_access('administer users')){
		if (module_exists('node_images')){
			drupal_set_message(t('Images and Room dimensions can be added after adding the listing.'));
		}else{
			drupal_set_message(t('Node Images module must be installed before you are able to add images to listings.'),'error');
		};
		return _listing_form($node);
	}else{
		drupal_set_message(t('You have exceeded your maximum posting for Listings.'));
		$form['#theme']='listing_blank_form';
		return $form;
	};
};
//----------------------------------------------------------------------------
/*
 * Use this theme so that the preview and submit buttons don't show up.
 */
function theme_listing_blank_form($node){
	return "Contact Site Administrator";
}
//----------------------------------------------------------------------------
/*
 * form_alter() hook to removew preview button
 */
function listing_form_alter($form_id, &$form){

	if($form_id == 'listing_node_form'){
		unset($form['preview']);
	};
};
//----------------------------------------------------------------------------
function _listing_form($node){
	// get metadata for this node type
	// need to get the title and body form
	$type = node_get_types('type', $node);

	$money_symbol = variable_get('listing_money_symbol','$');
    $measurement_unit = variable_get('listing_measurement_unit','metric');
    if ($measurement_unit == "metric"){ 
        $m_symbol = "m"."&sup2;";
    }else{
        $m_symbol = "ft"."&sup2;";
    };

	// needed for some ajax calls.
	drupal_add_js(drupal_get_path('module','listing').'/listing.js');

	$listing_type_array = rlistingapi_listing_type();
	if ($node->listing_type_id == ""){
		$node->listing_type_id = variable_get('listing_default_listing_type','');
	};
	$form['listing_type_id'] = array(
		'#type'		=> 'select',
		'#title'	=>  t('Listing Type'),
		'#default_value'	=> $node->listing_type_id,
		'#options'	=> $listing_type_array,
		'#weight'	=> 0,
	);
	$building_type_array = rlistingapi_building_type();
	$form['building_type_id'] = array(
		'#type'		=> 'select',
		'#title'	=>  t('Property Type'),
		'#default_value'	=> $node->building_type_id,
		'#options'	=> $building_type_array,
		'#weight'	=> 1,
	);
	$listing_array = rlistingapi_listing_status();
	$form['listing_status'] = array(
		'#type'		=> 'select',
		'#title'	=>  t('Listing Status'),
		'#default_value'	=> $node->listing_status,
		'#options'	=> $listing_array,
		'#weight'	=> 1,
	);
	if (user_access('administer listing')){
		$form['is_featured'] = array(
			'#type'		=> 'checkbox',
			'#title'	=>  t('Featured Listing'),
			'#default_value'	=> $node->is_featured,
			'#weight'	=> 1,
		);
	};
	$form['title'] = array(
		'#type'		=> 'textfield',
		'#title'	=> check_plain($type->title_label),
		'#required'	=> TRUE,
		'#default_value'	=> $node->title,
		'#weight'	=> 2,
	);
	$form['body'] = array(
		'#type'		=> 'textarea',
		'#title'	=> check_plain($type->body_label),
		'#default_value'	=> $node->body,
		'#rows'		=> 3,
		'#required'	=> TRUE,
		'#weight'	=> 3,
	);
	// ----------- Details ------------
	$form['details'] = array(
		'#type'		=> 'fieldset',
		'#title'	=> t('Property Details'),
		'#weight'	=> 3,
		'#collapsible'	=> FALSE,
		'#collapsed'	=> FALSE,
		'#tree'			=> TRUE,
	);
	$form['details']['mls_num'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t('MLS #'),
		'#default_value'	=> $node->mls_num,
		'#size'		=> 15,
		'#weight'	=> 0,
	);
	$form['details']['price'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t("Price ($money_symbol)"),
		'#default_value'	=> $node->price,
		'#required'	=>  TRUE,
		'#size'		=> 15,
		'#weight'	=> 2,
	);
	$form['details']['year_built'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t('Year Built'),
		'#description'	=>  t('eg. 1975'),
		'#default_value'	=> $node->year_built,
		'#required'	=>  FALSE,
		'#size'		=> 15,
		'#weight'	=> 4,
	);
	$form['details']['bedroom'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t('Bedrooms'),
		'#default_value'	=> $node->bedroom,
		'#required'	=>  FALSE,
		'#size'		=> 15,
		'#weight'	=> 8,
	);
	$form['details']['bathroom_full'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t('Full Bathrooms'),
		'#default_value'	=> $node->bathroom_full,
		'#required'	=>  FALSE,
		'#size'		=> 15,
		'#weight'	=> 8,
	);
	$form['details']['bathroom_half'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t('Half Bathrooms'),
		'#default_value'	=> $node->bathroom_half,
		'#size'		=> 15,
		'#weight'	=> 8,
	);
	// ----------- Details ------------
	$form['specs'] = array(
		'#type'		=> 'fieldset',
		'#title'	=> t('Property Details'),
		'#weight'	=> 3,
		'#collapsible'	=> FALSE,
		'#collapsed'	=> FALSE,
		'#tree'			=> TRUE,
	);
	$form['specs']['floor_space'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t("Floor Space ($m_symbol)"),
		'#default_value'	=> $node->floor_space,
		'#size'		=> 15,
		'#weight'	=> 1,
	);
	$form['specs']['land_size'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t("Land Size ($m_symbol)"),
		'#default_value'	=> $node->land_size,
		'#size'		=> 15,
		'#weight'	=> 2,
	);
	$form['specs']['storeys'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t('Storeys'),
		'#default_value'	=> $node->storeys,
		'#size'		=> 15,
		'#weight'	=> 3,
	);
	$form['specs']['maintenance_fee'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t("Maintenance Fee ($money_symbol)"),
		'#default_value'	=> $node->maintenance_fee,
		'#size'		=> 15,
		'#weight'	=> 3,
	);
	// ----------- Location ------------
	$form['location'] = array(
		'#type'		=> 'fieldset',
		'#title'	=> t('Property Location'),
		'#weight'	=> 3,
		'#collapsible'	=> FALSE,
		'#collapsed'	=> FALSE,
		'#tree'			=> TRUE,
	);
	if ($node->country_id ==""){
			$node->country_id = variable_get('listing_default_country',24);
	}; // canada
	$country_array = rlistingapi_country_array();
	$module_path = url('listing/set_prov/');
	$form['location']['country_id'] = array(
		'#type'		=> 'select',
		'#title'	=>  t('Country'),
		'#default_value'	=> $node->country_id,
		'#options'	=> $country_array,
		'#required'	=>  TRUE,
		'#weight'	=> 1,
		'#attributes'	=> array(
			'onChange'	=> "ajax_get('$module_path' + this.options[this.selectedIndex].value ,'prov_select')",
		),
	);
	$prov_array = rlistingapi_prov_array($node->country_id);
	if($node->prov_id == ""){
		$node->prov_id = variable_get('listing_default_prov','');
	};
	$form['location']['prov_id'] = array(
		'#type'		=> 'select',
		'#title'	=>  t('Prov/State'),
		'#default_value'	=> $node->prov_id,
		'#options'	=> $prov_array,
		'#DANGEROUS_SKIP_CHECK' => TRUE,
		'#weight'	=> 2,
		'#prefix'	=> '<div id="prov_select">',
		'#suffix'	=> '</div>',
	);
	$form['location']['suburb'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t('Suburb'),
		'#default_value'	=> $node->suburb,
		'#required'	=>  FALSE,
		'#size'		=> 24,
		'#weight'	=> 3,
		'#autocomplete_path'    => 'listing/suburb_autocomplete',
	);
	$form['location']['city_name'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t('City'),
		'#default_value'	=> $node->city_name,
		'#required'	=>  TRUE,
		'#size'		=> 24,
		'#weight'	=> 3,
		'#autocomplete_path'    => 'listing/city_autocomplete',
	);
	$form['location']['address'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t('Address'),
		'#default_value'	=> $node->address,
		'#required'	=>  TRUE,
		'#size'		=> 24,
		'#weight'	=> 4,
	);
	$form['location']['postal_zip'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t('Postal/Zip'),
		'#default_value'	=> $node->postal_zip,
		'#required'	=>  FALSE,
		'#size'		=> 15,
		'#weight'	=> 5,
	);
	$form['location']['region'] = array(
		'#type'		=> 'textfield',
		'#title'	=>  t('Region'),
		'#default_value'	=> $node->region,
		'#description'	=> t("i.e. Lower Mainland, Midlands ..."),	
		'#required'	=>  FALSE,
		'#size'		=> 24,
		'#weight'	=> 6,
		'#autocomplete_path'    => 'listing/region_autocomplete',
	);
	$map_options = array(
		'0'	=> t('show map'),
		'1'	=> t('Don\'t show map'),
	);
	$form['location']['no_map'] = array(
		'#title'	=> t('Map Setup'),
		'#type'		=> 'select',
		'#columns'	=> 4,
		'#default_value'	=> $node->no_map,
		'#options'	=> $map_options,
		'#weight'	=> 7,
	);
	// ----------- END Details ------------
	// ----------- Start Features ------------
	$SQL = "SELECT * FROM {rlisting_feature} ORDER BY feature";
	$result = db_query($SQL);
	while ($feature = db_fetch_object($result)){
		$feature_options[$feature->feature_id] = $feature->feature;
	};
	$form['feature'] = array(
		'#type'		=> 'fieldset',
		'#title'	=> t('Property Features'),
		'#weight'	=> 3,
		'#collapsible'	=> TRUE,
		'#collapsed'	=> FALSE,
		'#tree'			=> TRUE,
	);
	if (is_array($node->features)){
		$feature_values = array_keys($node->features);
	};
	$form['feature']['features'] = array(
		'#title'	=> t('Features'),
		'#type'		=> 'checkboxes',
		'#columns'	=> 4,
		'#default_value'	=> $feature_values,
		'#options'	=> $feature_options,
		'#theme'	=> 'columns_checkboxes',
		'#weight'	=> 6,
	);
			
	// ----------- End Features ------------
	$form['submit'] = array(
		'#type'		=> 'submit',
		'#title'	=>  t('Add Listing'),
		'#weight'	=> 9,
	);

	return $form;
};
//----------------------------------------------------------------------------
function theme_listing_node_form($form){

	$row1[] = array( 
				array (
					'data' => drupal_render($form['listing_type_id']),
					'valign' => 'top',
				),
				array (
					'data' => drupal_render($form['building_type_id']),
					'valign' => 'top',
				),
				array (
					'data' => drupal_render($form['is_featured']),
					'valign' => 'middle',
				),
				array (
					'data' => drupal_render($form['listing_status']),
					'valign' => 'top',
				),
	);
	$output = theme('table',array(),$row1,array('width'=>'100%'));

	$output .= drupal_render($form['title']);
	$output .= drupal_render($form['body']);

	$row[] = array (
				'data' => drupal_render($form['details']),
				'valign' => 'top',
				);
	$row[] = array (
				'data' => drupal_render($form['specs']),
				'valign' => 'top',
				);
	$row[] = array (
				'data' => drupal_render($form['location']),
				'valign' => 'top',
				);
	$rows[] = $row; $row = null; // reset
	$output .= theme('table',array(),$rows);
	$rows = null; // reset

	$output .= drupal_render($form);

	return $output;
};
