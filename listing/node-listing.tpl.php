<?php //phptemplate_comment_wrapper(NULL, $node->type); ?>

<div style="margin-bottom:1px" id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

<?php print $picture ?>
<div style="border:1px solid #999;padding:4px;">

<?php if ($page == 0): ?>
	<?php
		$listing_status = rlistingapi_listing_status($node->listing_status);
		echo '<div class="'.$listing_status.'-listing_status">';
		echo $listing_status;
		echo '</div>'
	?>
  <h2 ><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
<?php endif; ?>


  <div class="content" style="margin-top:0px;padding-top:0px">
<?php
	if ($teaser){
		if (strlen($node->content['openhouse']['#value']) > 5){
			$openhouse = "
					<fieldset class=\"collapsible collapsed\">
                    <legend>Open Houses</legend>
                        <div class=\"form-item\">
							".$node->content['openhouse']['#value']."
                        </div>
					</fieldset>
			";
		}else{
			$openhouse = "";
		};
		if (strlen($node->content['details']['#value']) > 5){
			$details = "
			<fieldset class=\"collapsible collapsed\">
                    <legend>Listing Details</legend>
                        <div class=\"form-item\">
				".$node->content['details']['#value']."
                        </div>
			</fieldset>
			";
		}else{
			$details = "";
		};
		if (strlen($node->content['features']['#value']) > 5){
			$features = "
			<fieldset class=\"collapsible collapsed\">
                    <legend>Listing Features</legend>
                        <div class=\"form-item\">
				".$node->content['features']['#value']."
                        </div>
			</fieldset>
			";
		}else{
			$features = "";
		};
		$price = variable_get('listing_money_symbol','$').number_format($node->price,2);
		if ($node->mls_num != ""){
			$mls_num = "MLS #:".$node->mls_num." <br />";
		}else{
			$mls_num = "";
		};
		echo "
		<table border=\"0\" width=\"100%\" style=\"padding:0px;margin:0px\">
		<tr>
			<td valign=\"top\"><strong>$price</strong>
				".$node->content['node_images']['#value']." 
				Beds: ".$node->bedroom." Baths: ".$node->bathroom_full."<br />
				$mls_num
				LID #: ".$node->nid."
			</td>
			<td valign=\"top\">
		".$node->content['agent']['#value']."
				<span class=\"listing-location\">".$node->content['location']['#value']."</span>
				".$node->content['body']['#value']."
				$openhouse
				$details
				$features
			</td>
		</tr>
		</table>
		";
	}else{
		print $content;
	};

?>
  </div>

  <div class="clear-block clear">
    <div class="meta">
    <?php if ($taxonomy): ?>
      <div class="terms"><?php print $terms ?></div>
    <?php endif;?>
    </div>

    <?php if ($links): ?>
      <div class="links"><?php print $links; ?></div>
    <?php endif; ?>
  </div>

</div>

</div>
