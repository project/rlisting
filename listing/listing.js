//----------------------------------------------------------------------------
// Initiate the xmlhttp object
//----------------------------------------------------------------------------
// create a boolean var to check for a valid IE instance
var xmlhttp = false;

// chekc if we are using IE
try{
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
} catch(e) {
        // if not, then use the odler active x object
        try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
                // else we must be using a no-IE browser
                xmlhttp = false;
        }
}

// if we are using a ono-IE browser, create a Javascipt instance of teh object
if (!xmlhttp && typeof XMLHttpRequest != 'undefinded'){
        xmlhttp = new XMLHttpRequest();
}
//----------------------------------------------------------------------------
function ajax_submit(serverPage,formName,divObject){

        var disPlayDiv = document.getElementById(divObject);
        // form object
        var myForm = document.getElementById(formName);
        var mydata = "";
        var fieldName = "";
        var value = "";
        for (var i=0; i < myForm.length; i++){
                fieldName = myForm[i].name;
                if (myForm[i].type == "checkbox"){
                        if (myForm[i].checked == true){
                                value = 1;
                        }else{
                                value = 0;
                        };
                }else{
                        value = myForm[i].value;
                };
                mydata += '&' + fieldName +'='+  value;
        }
        xmlhttp.open("POST",serverPage,true);
        xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xmlhttp.onreadystatechange = function(){
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
                        disPlayDiv.innerHTML = xmlhttp.responseText;
                }
        }
        xmlhttp.send(mydata);
}
//-----------------------------------------------------------------------------
function ajax_submit_append(serverPage,formName,divObject){
        // this appends the result to the a div object

        var disPlayDiv = document.getElementById(divObject);
        // form object
        var myForm = document.getElementById(formName);
        var mydata = "";
        var fieldName = "";
        var value = "";
        for (var i=0; i < myForm.length; i++){
                fieldName = myForm[i].name;
                if (myForm[i].type == "checkbox"){
                        if (myForm[i].checked == true){
                                value = 1;                        
			}else{
                                value = 0;
                        };
                }else{
                        value = myForm[i].value;
                }
                mydata += '&' + fieldName +'='+  value;
        }
        xmlhttp.open("POST",serverPage,true);
        xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xmlhttp.onreadystatechange = function(){
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
                        disPlayDiv.innerHTML = xmlhttp.responseText + disPlayDiv.innerHTML;
                }
        }
        xmlhttp.send(mydata);
}
//------------------------------------------------------------------------------
function ajax_get(serverPage,objID){
        var obj = document.getElementById(objID);

        xmlhttp.open("GET",serverPage,true);
        xmlhttp.onreadystatechange = function(){
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
                        obj.innerHTML = xmlhttp.responseText;
                        obj.style.display = "block";
                }
        }
        xmlhttp.send(null);
}
