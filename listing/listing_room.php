<?php
/**
*	theme the room section of the listing
*/
//---------------------------------------------------------------------------
function theme_listing_room($node,$teaser){
	drupal_add_js('misc/collapse.js');


	$room_array = $node->room;
	if (is_array($room_array)){
		$rows = array();
		while (list($index,$room) = each($room_array)){
			$rows[] = array(
				$room->room_name,
				$room->room_dimension,
				$room->notes,
			);
		};
		// show only if there are at least 1 open where the date hasn't 
		// passed yet.
		// setup the heading
		$heading = array('Room', 'Dimension','notes');
		if (count($rows) > 0){
			$room_tb = theme_table($heading,$rows);
			if ($teaser){
			// no need to show rooms in teaser
			}else{
				$output = "
					<fieldset>
					<legend>Rooms</legend>
						<div class=\"form-item\">
							$room_tb
						</div>
					</fieldset>
				";
			};
		};
	};
	
	return $output;


};

/*
* generates the form for editing rooms
*/
//---------------------------------------------------------------------------
function room_edit_form($node){
	global $user;
	$output = '<div id="attach-wrapper">';
	
	// generate a list of current open houses attached to this node.
	$SQL = "SELECT * FROM {rlisting_listing_room} WHERE nid = %d ";
	$room_result = db_query($SQL,$node->nid);
	if (db_num_rows($room_result)>0) $output .= drupal_get_form('room_list',$node,$room_result);

	$output .= drupal_get_form('room_edit_form_add',$node);

	// display the form for adding new openhouses. 
	$output .= '</div>';

	drupal_set_title(check_plain($node->title));
	return $output;
};
//---------------------------------------------------------------------------
function room_edit_form_add($node){

	$form['room'] = array(
		'#type'		=> 'fieldset',
		'#title'	=> t('Add new Room'),
	);
	$form['room']['room_name'] = array(
		'#title'	=> t('Room Name'),
		'#type'		=> 'textfield',
		'#default_value'	=> $node->room_name,
		'#weight'		=> -16,
		'#description'		=> t('Master Bedroom, Bathroom ... '),
		'#size'		=> 20,
	);
	$form['room']['room_dimension'] = array(
		'#title'	=> t('Room Dimension'),
		'#type'		=> 'textfield',
		'#default_value'	=> $node->room_dimension,
		'#weight'		=> -15,
		'#description'		=> t('eg. 18x20'),
		'#size'		=> 15,
	);
	$form['room']['notes'] = array(
		'#type'		=> 'textarea',
		'#title'		=> t('Notes'),
		'#description'		=> t('ie. large windows ... '),
		'#cols'		=> 40,
		'#rows'		=> 3,
		'#resizeable'		=> true,
	);
	$form['nid'] = array(
		'#type'		=> 'hidden',
		'#value'	=> $node->nid,
	);
	$form['room']['submit'] = array(
		'#type'		=> 'submit',
		'#value'	=> t('Add'),
	);

	return $form;
};
//---------------------------------------------------------------------------
function room_edit_form_add_validate($form_id,$form_values){
	if ($form_values['room_name'] == ""){
		form_set_error('room_name',t('Must provide a name for the room.'));
	};
};
function room_edit_form_add_submit($form_id,$form_values){
	$nid = $form_values['nid'];
	$notes = $form_values['notes'];

	$SQL = "INSERT INTO {rlisting_listing_room}
				(nid,room_name,room_dimension,notes) VALUES
				(%d,'%s','%s','%s')
	";
	db_query($SQL,$nid,$form_values['room_name'],
				$form_values['room_dimension'],$notes);
}

//---------------------------------------------------------------------------
function room_list($node,$room_result){

	$rooms = array();
  	$form['rows'] = array('#tree' => TRUE);
  	$delete_access = node_access('delete', $node);

	while ($record = db_fetch_object($room_result)){
		$rooms[$record->room_id] = $record;

		$form['rows'][$record->room_id]['room_name'] = array(
			//'#title'	=> t('Room Name'),
			'#type'		=> 'textfield',
			'#default_value'	=> $record->room_name,
			'#weight'		=> -14,
			'#description'		=> t('Master Bedroom, Bathroom ... '),
			'#size'		=> 20,
		);
		$form['rows'][$record->room_id]['room_dimension'] = array(
			//'#title'	=> t('Room Dimension'),
			'#type'		=> 'textfield',
			'#default_value'	=> $record->room_dimension,
			'#weight'		=> -15,
			'#description'		=> t('eg. 18x20'),
			'#size'		=> 15,
		);
		$form['rows'][$record->room_id]['notes'] = array(
			'#type'		=> 'textarea',
			'#description'		=> t('Notes for public to view'),
			'#default_value'		=> $record->notes,
			'#cols'		=> 25,
			'#rows'		=> 1,
			'#resizeable'		=> true,
		);
		$disabled = (!$delete_access);
	    $form['rows'][$record->room_id]['delete'] = 
					array('#type' => 'checkbox', '#disabled' => $disabled);	
		
	};

	$form['nid'] = array('#type'=>'value','#value'=>$node->nid);
	$form['save'] = array('#type'=>'submit','#value'=>t('Save changes'));
	$form['rooms'] = array(
		'#type'	=> 'value',
		'#value'	=> $rooms,
	);

	return $form;

};
function theme_room_list($form){
	$header = array(t('Room'),t('Dimensions'),t('Notes'),t('Delete'));

	$rows = array();
	foreach($form['rooms']['#value'] as $id=>$room){
		$row = array();
		$row[] = drupal_render($form['rows'][$room->room_id]['room_name']);
		$row[] = drupal_render($form['rows'][$room->room_id]['room_dimension']);
		$row[] = drupal_render($form['rows'][$room->room_id]['notes']);
		$row[] = drupal_render($form['rows'][$room->room_id]['delete']);
		$rows[] = $row;
	};

  	$output = '<fieldset><legend>'.t('Listing Rooms').'</legend>';
	$output .= theme('table',$header,$rows);
	$output .= drupal_render($form);
  	$output .= '</fieldset>';
	
	return $output;	
}
//---------------------------------------------------------------------------
function room_list_submit($form_id,$form_values){
	global $user;
	$node = node_load($form_values['nid']);

	if (!node_access('update',$node)) return;
	$delete_access = node_access('delete', $node);
	foreach ($form_values['rows'] as $id => $edit){
		if ($edit['delete']){
			// if user hos no delete access to node then move on.
			if (!$delete_access ) continue;

			$SQL = "DELETE FROM {rlisting_listing_room} WHERE room_id=%d AND nid=%d";
			db_query($SQL,$id,$node->nid);
		}else{
			$SQL = "UPDATE {rlisting_listing_room} SET
					room_name = '%s', room_dimension = '%s', notes = '%s'
					WHERE room_id=%d AND nid=%d
			";

			db_query($SQL,$edit['room_name'],
				$edit['room_dimension'],$edit['notes'],$id,$node->nid);
						
		};
	};
		drupal_set_message(t('The changes have been saved.'));
}
