Drupal rListing module:
-----------------------
Author - Ken Yiem (kenuck@gmail.com)
Requires - Drupal 5.x
License - GPL (see LICENSE)

Overview:
---------
rListing is a real estate module that plugs into Drupal 5.x. The module can be used by individual agents as their personal listing site or by agencies to allow their agents to login and post their own listings.

Drupal roles can be used to assign user types for each member (ie. Agents, Normal Users . ). This means that administrators can assign permissions to specific type of users ie. Who can post listings, who can view listing and so on.

Administrators will also have the ability to limit the number of posting on a per user basis. They also have the ability to add/remove property features and building type.

Here are some features of the module.
- posting of Listing nodes
- google maps integration
- favorite list (printing and emailing)
- attach photos to each listing
- search block
- featured block
- open house feature (add/remove/update) for each listing.


Thanks to user contribution modules:
- jscalendar
- node_images
- thickbox

Installation
-------------
Refer to INSTALL.txt

Configuration:
--------------
Refer to INSTALL.txt

Documentation:
-----------------
http://www.netriftsolutions.com/?q=node/58

Contributions:
--------------
* thickbox module - Fredrik Jonsson
* jscalendar - unkown
* node_images - Stefano Mallozzi (www.casepertutti.com)
